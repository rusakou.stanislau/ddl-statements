PGDMP         3                |           climbersdatabase     15.5 (Ubuntu 15.5-1.pgdg20.04+1)     15.5 (Ubuntu 15.5-1.pgdg20.04+1)     !           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            "           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            #           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            $           1262    16425    climbersdatabase    DATABASE     |   CREATE DATABASE climbersdatabase WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.UTF-8';
     DROP DATABASE climbersdatabase;
                postgres    false            �            1259    16471    climbers    TABLE     �  CREATE TABLE public.climbers (
    climberid integer NOT NULL,
    firstname character varying(100) NOT NULL,
    lastname character varying(100) NOT NULL,
    dateofbirth date,
    gender character(1),
    record_ts date DEFAULT CURRENT_DATE,
    CONSTRAINT climbers_dateofbirth_check CHECK ((dateofbirth > '2000-01-01'::date)),
    CONSTRAINT climbers_gender_check CHECK ((gender = ANY (ARRAY['M'::bpchar, 'F'::bpchar, 'O'::bpchar])))
);
    DROP TABLE public.climbers;
       public         heap    bob    false            �            1259    16470    climbers_climberid_seq    SEQUENCE     �   CREATE SEQUENCE public.climbers_climberid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.climbers_climberid_seq;
       public          bob    false    215            %           0    0    climbers_climberid_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.climbers_climberid_seq OWNED BY public.climbers.climberid;
          public          bob    false    214            �            1259    16493    climbs    TABLE       CREATE TABLE public.climbs (
    climbid integer NOT NULL,
    climberid integer NOT NULL,
    routeid integer NOT NULL,
    climbdate date DEFAULT CURRENT_DATE,
    record_ts date DEFAULT CURRENT_DATE,
    CONSTRAINT climbs_climbdate_check CHECK ((climbdate > '2000-01-01'::date))
);
    DROP TABLE public.climbs;
       public         heap    bob    false            �            1259    16492    climbs_climbid_seq    SEQUENCE     �   CREATE SEQUENCE public.climbs_climbid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.climbs_climbid_seq;
       public          bob    false    219            &           0    0    climbs_climbid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.climbs_climbid_seq OWNED BY public.climbs.climbid;
          public          bob    false    218            �            1259    16481    routes    TABLE     �   CREATE TABLE public.routes (
    routeid integer NOT NULL,
    routename character varying(255) NOT NULL,
    difficulty character varying(10) NOT NULL,
    location character varying(255) NOT NULL,
    record_ts date DEFAULT CURRENT_DATE
);
    DROP TABLE public.routes;
       public         heap    bob    false            �            1259    16480    routes_routeid_seq    SEQUENCE     �   CREATE SEQUENCE public.routes_routeid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.routes_routeid_seq;
       public          bob    false    217            '           0    0    routes_routeid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.routes_routeid_seq OWNED BY public.routes.routeid;
          public          bob    false    216            w           2604    16474    climbers climberid    DEFAULT     x   ALTER TABLE ONLY public.climbers ALTER COLUMN climberid SET DEFAULT nextval('public.climbers_climberid_seq'::regclass);
 A   ALTER TABLE public.climbers ALTER COLUMN climberid DROP DEFAULT;
       public          bob    false    215    214    215            {           2604    16496    climbs climbid    DEFAULT     p   ALTER TABLE ONLY public.climbs ALTER COLUMN climbid SET DEFAULT nextval('public.climbs_climbid_seq'::regclass);
 =   ALTER TABLE public.climbs ALTER COLUMN climbid DROP DEFAULT;
       public          bob    false    219    218    219            y           2604    16484    routes routeid    DEFAULT     p   ALTER TABLE ONLY public.routes ALTER COLUMN routeid SET DEFAULT nextval('public.routes_routeid_seq'::regclass);
 =   ALTER TABLE public.routes ALTER COLUMN routeid DROP DEFAULT;
       public          bob    false    216    217    217                      0    16471    climbers 
   TABLE DATA           b   COPY public.climbers (climberid, firstname, lastname, dateofbirth, gender, record_ts) FROM stdin;
    public          bob    false    215   N                  0    16493    climbs 
   TABLE DATA           S   COPY public.climbs (climbid, climberid, routeid, climbdate, record_ts) FROM stdin;
    public          bob    false    219   �                  0    16481    routes 
   TABLE DATA           U   COPY public.routes (routeid, routename, difficulty, location, record_ts) FROM stdin;
    public          bob    false    217   �        (           0    0    climbers_climberid_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.climbers_climberid_seq', 2, true);
          public          bob    false    214            )           0    0    climbs_climbid_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.climbs_climbid_seq', 2, true);
          public          bob    false    218            *           0    0    routes_routeid_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.routes_routeid_seq', 2, true);
          public          bob    false    216            �           2606    16479    climbers climbers_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.climbers
    ADD CONSTRAINT climbers_pkey PRIMARY KEY (climberid);
 @   ALTER TABLE ONLY public.climbers DROP CONSTRAINT climbers_pkey;
       public            bob    false    215            �           2606    16501    climbs climbs_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.climbs
    ADD CONSTRAINT climbs_pkey PRIMARY KEY (climbid);
 <   ALTER TABLE ONLY public.climbs DROP CONSTRAINT climbs_pkey;
       public            bob    false    219            �           2606    16489    routes routes_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_pkey PRIMARY KEY (routeid);
 <   ALTER TABLE ONLY public.routes DROP CONSTRAINT routes_pkey;
       public            bob    false    217            �           2606    16491    routes routes_routename_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_routename_key UNIQUE (routename);
 E   ALTER TABLE ONLY public.routes DROP CONSTRAINT routes_routename_key;
       public            bob    false    217            �           2606    16502    climbs climbs_climberid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.climbs
    ADD CONSTRAINT climbs_climberid_fkey FOREIGN KEY (climberid) REFERENCES public.climbers(climberid);
 F   ALTER TABLE ONLY public.climbs DROP CONSTRAINT climbs_climberid_fkey;
       public          bob    false    219    3202    215            �           2606    16507    climbs climbs_routeid_fkey    FK CONSTRAINT        ALTER TABLE ONLY public.climbs
    ADD CONSTRAINT climbs_routeid_fkey FOREIGN KEY (routeid) REFERENCES public.routes(routeid);
 D   ALTER TABLE ONLY public.climbs DROP CONSTRAINT climbs_routeid_fkey;
       public          bob    false    219    3204    217               N   x�3�t��LN�����+���4200�50�54�tr�LtLu̹�8��3s*9�sRA��t�t��9}�U��qqq P�{         &   x�3�4B##]S]$�9�H!`�,���� 1�	�         T   x�3��HU���+Qp�I���4ճ�t�����,��4202�50�50�2���L��L�(Q��y�y�a���ũ@���jc���� ��     